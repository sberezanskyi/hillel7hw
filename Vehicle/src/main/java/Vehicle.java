public class Vehicle {

    private String name;
    private int engine;
    private int doors;
    private double weight;


    public Vehicle(String name, int engine, int doors, double weight) {
        this.name = name;
        this.engine = engine;
        this.doors = doors;
        this.weight = weight;


    }

    public void startVehicle() {
        System.out.println("Car is now on");
    }

    public String getName() {
        return name;
    }

    public int getEngine() {
        return engine;
    }

    public int getDoors() {
        return doors;
    }

    public double getWeight() {
        return weight;
    }

}