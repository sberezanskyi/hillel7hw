public class Model_S extends Car {

    private double MaxBatteryCharge;

    public Model_S(double MaxBatteryCharge) {
        super("Tesla Model S", 400, 4, 2000, 4, 4);
        this.MaxBatteryCharge = MaxBatteryCharge;
    }

    public void checkBatteryCharge(double SpentBatteryCharge) {
        System.out.println((MaxBatteryCharge - SpentBatteryCharge));
    }

    private void electricEngine() {
        System.out.println("Model_S.electricEngine() called");
        String carPowerSource = "Tesla Electric Engine activated!";
        System.out.println(carPowerSource);
    }

    @Override
    public void startVehicle() {
        System.out.println("Model_S.startVehicle called");
        electricEngine();
        super.startVehicle();
    }

    public double getMaxBatteryCharge() {
        return MaxBatteryCharge;
    }
}