//7. Start with a base class of a Vehicle, then create a Car class that inherits from this base class.
//        Finally, create another class, a specific type of Car that inherits from the Car class.
//        You should be able to hand steering, changing gears, and moving(speed in other words).
//        You will want to decide where to put the appropriate state and behaviours(fields and methods).
//        As mentioned above, changing gears, increasing/decreasing speed should be included.
//        For you specific type of vehicle you will want to add something specific for that type of car.

public class Main {

    public static void main(String[] args) {
        Car car = new Car("MASK",250,4,1500, 4, 2);


        car.handSteering('l');
        car.gearChange('N');
        car.Accelerate(45);
        car.Decelerate(50);

        Model_S model_s = new Model_S(3000);

        System.out.println("Car has " + model_s.getDoors() + " doors");
        System.out.println(model_s.getName());


        model_s.Accelerate(84);

        System.out.println(model_s.getWeight());

    }
}