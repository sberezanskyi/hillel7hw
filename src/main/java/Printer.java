//////6. Create a class and demonstrate proper encapsulation techniques
//////        the class will be called Printer
//////        It will simulate a real Computer printer
//////        It should have fields for toner level, number of pages printed, and also
//////        whether its a duplex printer (capable of printing on both sides of the paper).
//////        Add methods to fill up the toner (up to a maximum of 100%), another method to
//////        simulate printing a page(which should increase the number of pages printed).
//
//public class Printer {
//
//    private String manufacturer;
//    private String model;
//    private boolean isColor;
//    private String type;
//    private boolean isDuplex;
//    private int tonerLevelBlack ;
//    private int tonerLevelMagenta;
//    private int tonerLevelYellow;
//    private int tonerLevelBlue;
//    private int printedPages;
//
//
//   public String getManufacturer() {
//        return manufacturer;
//    }
//
//    public String getModel() {
//        return model;
//    }
//
//    public boolean getIsColor() {
//        return isColor;
//    }
//
//    public int getTonerLevelBlack() {
//        return tonerLevelBlack;
//    }
//
//    public boolean isDuplex() {
//        return isDuplex;
//    }
//
//    public int getTonerLevelMagenta() {
//        return tonerLevelMagenta;
//    }
//
//    public int getTonerLevelYellow() {
//        return tonerLevelYellow;
//    }
//
//
//    public int getTonerLevelBlue() {
//        return tonerLevelBlue;
//    }
//
//    public int getPrintedPages() {
//        return printedPages;
//    }
//
//    public void setColor(boolean color) {
//        isColor = color;
//    }
//
//    public void setTonerLevelBlack(int tonerLevelBlack) {
//        this.tonerLevelBlack = tonerLevelBlack;
//    }
//
//    public void setDuplex(boolean duplex) {
//        isDuplex = duplex;
//    }
//
//    public void setPrintedPages(int printedPages) {
//        this.printedPages = printedPages;
//    }
//
//    public int zapravkaToner(int blackColor) {
//        this.tonerLevelBlack = this.tonerLevelBlack + blackColor;
//
//        if (this.tonerLevelBlack > 100) {
//            int result = blackColor + 100 - tonerLevelBlack;
//            System.out.println("Black toner zapravlen");
//
//        } else {
//            System.out.println("Black toner is " + this.tonerLevelBlack);
//        }
//
//        return 0;
//    }
//
//
//    public static void main(String[] args) {
//
//        Printer printer = new Printer();
//        printer.isColor = false;
//        printer.tonerLevelBlack = 50;
//        printer.printedPages = 10;
//        printer.zapravkaToner(15);
//
//        System.out.println("Count of pages: " + printer.getPrintedPages());
//        System.out.println("After printing, let's fill up toner "+ "\n" + printer.zapravkaToner(15));
//
//
//    }
//
//}
